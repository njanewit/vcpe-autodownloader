require "mechanize"
require "nokogiri"
require 'fileutils'
require 'filemagic'
require 'mime/types'

cookie_val = 'C60327C27D2C4284019D38FF6FBAE4E8.hbp1'

a = Mechanize.new { |agent| agent.user_agent_alias = "Mac Safari" }
a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
cookie = Mechanize::Cookie.new :domain => 'vcpe.forio.com', :name => 'JSESSIONID',:value => cookie_val, :path => '/simulate'
a.agent.cookie_jar << cookie

doc = Nokogiri::HTML(a.get('https://vcpe.forio.com/simulate/hbs/vcpe/simulation/index.html').body)

if doc.css('#btnLogin').empty? then
  puts "login success"
else
  puts "login failed"
end

