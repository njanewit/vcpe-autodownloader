require "mechanize"
require "nokogiri"
require 'fileutils'
require 'filemagic'
require 'mime/types'
require 'json'
require 'readline'
require 'highline/import'

#DEBUG FLAG
debug_mode = false

require File.expand_path(File.dirname(__FILE__) + '/credentials.rb')
user = ENV['USER']
pass = ENV['PASS']  

login_url = 'https://vcpe.forio.com/simulate/api/authentication/hbs/vcpe?&skip_redirect=true'

a = Mechanize.new { |agent| agent.user_agent_alias = "Mac Safari" }
a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE

a.post(login_url, {"email" => user, "password" => pass, "user_action" => "login"}, {'Content-Type' => "application/x-www-form-urlencoded"})

doc = Nokogiri::HTML(a.get('https://vcpe.forio.com/simulate/hbs/vcpe/simulation/index.html').body)

if doc.css('#btnLogin').empty? then
	puts "login success"
	found_companies = JSON.parse(a.get('https://vcpe.forio.com/simulate/custom/vcpe/api/hbs/vcpe/found').body)
	company_csv = ''

	# need to implement company detail page
	# Request URL: https://vcpe.forio.com/simulate/custom/vcpe/api/hbs/vcpe/companies/35

	i = 0
	found_companies['data'].each do |company|
		# prepare keys_csv and values_csv lines
		keys_csv = ''
		values_csv = ''
		company_id = 0
		company_name = ""
		company.each do |key, value|
			# need to special case created and company
			if (key.to_s == 'created')
				keys_csv += key.to_s + ','
				values_csv += value['year'].to_s + ','
			elsif (key.to_s == 'company')
				company[key].each do |dkey, dvalue|
					keys_csv += dkey.to_s + ','
					values_csv += dvalue.to_s + ','
					company_id = dvalue if dkey == 'id'
					company_name = dvalue if dkey == 'name'
				end
			else
				keys_csv += key.to_s + ','
				values_csv += value.to_s + ','
			end
		end

		#load company overview
		print "[" + ("%03d" % (i+1)) + "/"+ ("%03d" % found_companies['data'].count ) + "] Loading " + company_name + " (" + company_id.to_s + ")..."
		company_metrics = JSON.parse(a.get('https://vcpe.forio.com/simulate/custom/vcpe/api/hbs/vcpe/companies/'+company_id.to_s).body)
		company_metrics['data'].each do |mkey, mvalue|
			if mvalue.kind_of?(Array)
				#Assumes that variables are indexed by year and there will be 10 years max
				for j in 0..9
					keys_csv += mkey.to_s + "_" +j.to_s+ ','
	                                values_csv += mvalue[j].to_s + ','
				end
			elsif mvalue.kind_of?(Hash)
				mvalue.each do |hkey, hvalue|
					keys_csv += mkey.to_s + "_" +hkey.to_s+ ','
                                        values_csv += hvalue.to_s + ','
				end
			else
				keys_csv += mkey.to_s + ','
        	                values_csv += mvalue.to_s + ','
			end
		end
		puts "Done"

		# actually output to file
		if (i == 0)
			company_csv += keys_csv + "\n"
		end
		company_csv += values_csv + "\n"
		i = i + 1
		
		if debug_mode
			puts "DEBUG MODE: Exiting after one item"
			break
		end
	end

	#File.delete('found_referred.csv') if File.exist?('found_referred.csv')
	File.write('found_referred.csv', company_csv)
	puts "Wrote everything to found_referred.csv!"
else
	puts "login failed"
end

